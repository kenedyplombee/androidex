package com.example.main1;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.Toast;
import android.widget.ToggleButton;

public class MyToggleButton extends ToggleButton {
    public MyToggleButton(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    public MyToggleButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public MyToggleButton(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public MyToggleButton(Context context) {
        super(context);
    }
    @Override
    public boolean onTouchEvent(MotionEvent event){
        Bitmap bmp = drawableToBitmap(getBackground());
        int pixel = bmp.getPixel(Math.round(event.getX()), Math.round(event.getY()));

        int alpha = Color.alpha(pixel);

        String s = "";

        switch (this.getContext().getResources().getResourceName(getId())){
            case "com.example.main1:id/toggleButton1":
                s = "1";
                break;
            case "com.example.main1:id/toggleButton2":
                s = "2";
                break;
            case "com.example.main1:id/toggleButton3":
                s = "3";
                break;
            case "com.example.main1:id/toggleButton4":
                s = "4";
                break;
            case "com.example.main1:id/toggleButton5":
                s = "5";
                break;
        }

        if (alpha < 127){
            return false;
        }else {
            Toast.makeText(this.getContext(), s, Toast.LENGTH_SHORT).show();
            return super.onTouchEvent(event);
        }
    }

    public Bitmap drawableToBitmap(Drawable drawable) {
        Bitmap bitmap = Bitmap.createBitmap(Math.round(this.getContext().getResources().getDimension(R.dimen.buttonWidth)),
                Math.round(this.getContext().getResources().getDimension(R.dimen.buttonHeight))
                , Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        drawable.setBounds(0,0, canvas.getWidth(), canvas.getHeight());
        drawable.draw(canvas);
        return bitmap;
    }

}
